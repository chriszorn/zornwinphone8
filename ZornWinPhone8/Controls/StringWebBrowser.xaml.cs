﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone8.Controls
{
    public partial class StringWebBrowser : UserControl
    {
        private static readonly DependencyProperty _htmlSourceProperty = DependencyProperty.Register
        (
          "HtmlSource", typeof(string), typeof(StringWebBrowser), new PropertyMetadata(String.Empty, HtmlSourcePropertyChanged)
        );

        public string HtmlSource
        {
            get { return (string)GetValue(_htmlSourceProperty); }
            set { SetValue(_htmlSourceProperty, value); }
        }

        public WebBrowser WebBrowser
        {
            get { return Browser; }
        }

        private static void HtmlSourcePropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            var control = (StringWebBrowser)sender;
            control.UpdateControl();
        }

        public StringWebBrowser()
        {
            InitializeComponent();
        }

        public void UpdateControl()
        {
            Browser.Navigated -= Browser_Navigated;

            if (string.IsNullOrWhiteSpace(HtmlSource))
            {
                Browser.Visibility = Visibility.Collapsed;
                Browser.NavigateToString("about:blank");
            }
            else
            {
                Browser.Navigated += Browser_Navigated;
                Browser.NavigateToString(HtmlSource);
            }
        }

        void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            Browser.Visibility = Visibility.Visible;
        }
    }
}
