﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone8.Controls
{
    public partial class RemoteGifImage : UserControl
    {
        private const string _pageHtml = "<html><head><style>body, div, img { margin:0; padding:0; }</style><script type=\"text/javascript\">window.onload = function () { var elem = document.getElementById('content'); var height = 0; if(elem != null) { height = elem.scrollHeight; document.getElementById('body').removeChild(document.getElementById('content')); } window.external.Notify(height + ''); }</script></head><body id=\"body\"><div id=\"unloaded-content\" style=\"width:100px; max-height:10px; overflow:hidden;\"><img src=\"#IMAGEURL#\" style=\"width:100%\" onload=\"document.getElementById('unloaded-content').id = 'content';\" /></div><img src=\"#IMAGEURL#\" style=\"width:100%\" /></body></html>";
        private const string _urlReplaceValue = "#IMAGEURL#";
        private const int maxHeight = 600;

        private static readonly DependencyProperty _imageUrlProperty = DependencyProperty.Register
        (
          "ImageUrl", typeof(string), typeof(RemoteGifImage), new PropertyMetadata(String.Empty, ImageUrlPropertyChanged)
        );

        public string ImageUrl
        {
            get { return (string)GetValue(_imageUrlProperty); }
            set { SetValue(_imageUrlProperty, value); }
        }

        private static void ImageUrlPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            var control = (RemoteGifImage)sender;
            control.Update();
        }

        public RemoteGifImage()
        {
            InitializeComponent();

            GifBrowser.IsHitTestVisible = false;
            GifBrowser.Height = 0;
            GifBrowser.IsScriptEnabled = true;
            GifBrowser.ScriptNotify += Browser_ScriptNotify;
            //GifBrowser.Loaded="Browser_Loaded"

            GifBrowser.NavigationFailed += GifBrowser_NavigationFailed;

            this.Unloaded += RemoteGifImage_Unloaded;
        }

        void GifBrowser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new NotImplementedException();
        }

        void RemoteGifImage_Unloaded(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        public void Clear()
        {
            GifBrowser.ScriptNotify -= Browser_ScriptNotify;
            GifBrowser.NavigationFailed -= GifBrowser_NavigationFailed;
            GifBrowser.Height = 0;
            GifBrowser.Visibility = Visibility.Collapsed;
            GifBrowser.NavigateToString("about:blank");
            LayoutRoot.Children.Remove(GifBrowser);
            //GifBrowser = null;
        }

        private void Update()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("UpdateImage " + ImageUrl);
                GifBrowser.NavigateToString("about:blank");
                string html = _pageHtml.Replace(_urlReplaceValue, ImageUrl);
                GifBrowser.NavigateToString(html);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Resizes the WebBrowser to the height of the image so it appears as if it is a normal Image control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Browser_ScriptNotify(object sender, NotifyEventArgs e)
        {
            int height;
            double newHeight;
            WebBrowser thisBrowser = (WebBrowser)sender;

            try
            {
                height = Convert.ToInt32(e.Value);
                if (height > 0)
                {
                    newHeight = thisBrowser.ActualWidth * (height / 100.0);
                    newHeight -= 2; // Take off two pixels to get rid of whitespace at the bottom
                    newHeight = Math.Max(0, Math.Min(maxHeight, newHeight));
                    thisBrowser.Height = newHeight;
                    LayoutRoot.Visibility = Visibility.Visible;
                }
                else
                {
                    LayoutRoot.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception) 
            {
                thisBrowser.Height = 10;
                LayoutRoot.Visibility = Visibility.Collapsed;
            }
        }

        private void Browser_Loaded(object sender, RoutedEventArgs e)
        {
            Update();
        }
    }
}
