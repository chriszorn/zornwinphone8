﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Imaging;

namespace ZornWinPhone8
{
    public class HtmlElement
    {
        public string ObjectClassName;
        public string Source;
        public string Text;
        public string Url;
        public string Tag;
        public string Class;
    }
}
