﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone8
{
    public class GenericGroup<T> : List<T>
    {
        public string Key
        {
            get;
            set;
        }

        public GenericGroup(string name, IEnumerable<T> items)
        {
            this.Key = name;

            foreach (T item in items)
                this.Add(item);
        }
    }
}
