﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZornWinPhone8
{
    public static class ThemeUtil
    {
        public static string ThemeBackgroundColor
        {
            get { return IsDarkThemeEnabled ? "#000;" : "#fff"; }
        }

        public static string ThemeFontColor
        {
            get { return IsDarkThemeEnabled ? "#fff;" : "#000"; }
        }

        public static bool IsDarkThemeEnabled
        {
            get
            {
                return ((Visibility)Application
                  .Current
                  .Resources["PhoneDarkThemeVisibility"] == Visibility.Visible);
            }
        }
    }
}
