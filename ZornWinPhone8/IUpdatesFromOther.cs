﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone8
{
    public interface IUpdatesFromOther
    {
        void UpdateFromOther(object other);
    }
}
