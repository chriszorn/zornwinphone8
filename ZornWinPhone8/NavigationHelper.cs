﻿using System;
using System.Windows;

namespace ZornWinPhone8
{
    public static class NavigationHelper
    {
        public static void Navigate(Uri uri)
        {
            if (uri == null) { return; }

            try
            {
                (Application.Current.RootVisual as Microsoft.Phone.Controls.PhoneApplicationFrame).Navigate(uri);
            }
            catch { }
        }

        public static void NavigatToAboutPage()
        {
            Navigate(new Uri("/ZornWinPhone8;component/About.xaml", UriKind.Relative));
        }

        public static void NavigateToImageView(System.Windows.Media.ImageSource imageSource)
        {
            if (imageSource == null) { return; }

            ZornWinPhone8.ImagePage.ImageSourceArg = imageSource;
            Navigate(new Uri("/ZornWinPhone8;component/ImagePage.xaml", UriKind.Relative));
        }

        public static void NavigateToShareViaPage()
        {
            Navigate(new Uri("/ZornWinPhone8;component/ShareViaPage.xaml", UriKind.Relative));
        }
    }
}
