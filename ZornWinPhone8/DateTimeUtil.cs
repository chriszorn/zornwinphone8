﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone8
{
    public static class DateTimeUtil
    {
        public static DateTime StartOfWeek(DateTime dateTime)
        {
            return StartOfWeek(dateTime, DayOfWeek.Sunday);
        }

        public static DateTime StartOfWeek(DateTime dateTime, DayOfWeek dayOfWeek)
        {
            return dateTime.Date.AddDays( -ZMath.ArithmeticModulus((int)dateTime.DayOfWeek - (int)dayOfWeek, 7) );
        }

        public static DateTime StartOfMonth(DateTime dateTime)
        {
            return StartOfMonth(dateTime, 1);
        }

        public static DateTime StartOfMonth(DateTime dateTime, int dayOfMonth)
        {
            return dateTime.Date.AddDays(-ZMath.ArithmeticModulus((int)dateTime.Day - (int)dayOfMonth, DateTime.DaysInMonth(dateTime.Year, dateTime.Month)));
        }
    }
}
