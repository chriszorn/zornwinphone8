﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Phone.Controls;

namespace ZornWinPhone8
{
    public class SaveCancelViewModel : ViewModelBase
    {
        public virtual void CancelButton_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        public virtual void SaveButton_Click(object sender, EventArgs e)
        {
            GoBack();
        }
    }
}
