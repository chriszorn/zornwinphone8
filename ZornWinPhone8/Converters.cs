﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Data;

namespace ZornWinPhone8.Converters
{
    public class ConverterBase : IValueConverter
    {
        public virtual object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class BoolInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }
    }

    public class BoolToInverse : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !(bool)value;
        }
    }

    public class DoubleGreaterThanZeroToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double val = System.Convert.ToDouble(value);
            return val > 0 ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class IEnumerableToCount : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = ((IEnumerable)value).Cast<object>().ToList();
            return result.Count();
        }
    }

    public class IEnumerableInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) { return Visibility.Visible; }

            var result = ((IEnumerable)value).Cast<object>().ToList();
            return (result == null || result.Count == 0) ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class IEnumerableToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) { return Visibility.Collapsed; }

            var result = ((IEnumerable)value).Cast<object>().ToList();
            return (result != null && result.Count() > 0) ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class IsAdEnabledToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
#if FREE_WITH_ADS
            //return Ads.IsAdEnabled ? Visibility.Visible : Visibility.Collapsed;
            return Visibility.Visible;
#else
            return Visibility.Collapsed;
#endif
        }
    }

    public class IndexToCardinal : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)value + 1;
        }
    }

    public class LongGreaterThanZeroToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long val = System.Convert.ToInt64(value);
            return val > 0 ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    /// <summary>
    /// E.g. 1827389 => 1,827,389
    /// </summary>
    public class ObjectToCommaSeparatedNumberString : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return String.Format("{0:N0}", value);
        }
    }

    public class ObjectInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class ObjectToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class StringInverseToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.IsNullOrWhiteSpace((string)value) ? Visibility.Collapsed : Visibility.Visible;
        }
    }

    public class StringToLower : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                return value.ToString().ToLowerInvariant();
            }

            return string.Empty;
        }
    }

    public class StringToUpper : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                return value.ToString().ToUpperInvariant();
            }

            return string.Empty;
        }
    }

    public class StringToVisibility : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((string)value != null && (string)value != String.Empty) ? Visibility.Visible : Visibility.Collapsed;
        }
    }

    public class ToggleSwitchYesNo : ConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? "Yes" : "No";
        }
    }
}
