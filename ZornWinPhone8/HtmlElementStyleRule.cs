﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace ZornWinPhone8
{
    public class HtmlElementStyleRule
    {
        public string Tag = string.Empty;
        public string Class = string.Empty;
        public FontStyle FontStyle = FontStyles.Normal;
        public double FontSize;
        public FontWeight FontWeight = FontWeights.Normal;
        public string FontFamily;
        public Brush Foreground;

        public void Apply(Run run)
        {
            run.FontStyle = FontStyle;
            run.FontWeight = FontWeight;
            if (FontSize != 0)
            {
                run.FontSize = FontSize;
            }
            if (!string.IsNullOrWhiteSpace(FontFamily))
            {
                run.FontFamily = new FontFamily(FontFamily);
            }
        }

        public void Apply(Paragraph paragraph)
        {
            paragraph.FontStyle = FontStyle;
            paragraph.FontWeight = FontWeight;
            if (FontSize != 0)
            {
                paragraph.FontSize = FontSize;
            }
            if (!string.IsNullOrWhiteSpace(FontFamily))
            {
                paragraph.FontFamily = new FontFamily(FontFamily);
            }
            if (Foreground != null)
            {
                paragraph.Foreground = Foreground;
            }
        }
    }

    
}
