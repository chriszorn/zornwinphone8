﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone8
{
    public class AppMenuItem : ModelBase
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(); }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; NotifyPropertyChanged(); }
        }

        private string _iconImage;
        public string IconImage
        {
            get { return _iconImage; }
            set { _iconImage = value; NotifyPropertyChanged(); }
        }

        public Uri Destination;
    }
}
