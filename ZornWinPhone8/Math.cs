﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone8
{
    public static class ZMath
    {
        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        public static int ArithmeticModulus(int x, int m)
        {
            return x < 0 ? ((x % m) + m) % m : x % m;
        }
    }
}
