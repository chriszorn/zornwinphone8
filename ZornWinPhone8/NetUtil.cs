﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Win8WinPhone.CodeShare.Extensions;

namespace ZornWinPhone8
{
    public static class NetUtil2
    {
        public static async Task<string> HttpGetRequest(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = HttpMethod.Get;
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        public static async Task<byte[]> HttpGetRequestBytes(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = HttpMethod.Get;
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                MemoryStream ms = new MemoryStream();
                using (Stream input = response.GetResponseStream())
                {
                    input.CopyTo(ms);
                }
                return ms.ToArray();
            }
            catch
            {
                return default(byte[]);
            }
        }

        public static async Task<Stream> HttpGetRequestStream(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = HttpMethod.Get;
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                return response.GetResponseStream();
            }
            catch
            {
                return default(Stream);
            }
        }

        public static async Task<string> HttpCookieGetRequest(string url, CookieCollection cookies)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = HttpMethod.Get;
                AddCookies(ref request, cookies);
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        public static async Task<T> HttpGetRequestToDeserializedJson<T>(string url)
        {
            T ret = default(T);
            string json = await HttpGetRequest(url);

            if (json != string.Empty)
            {
                try
                {
                    ret = JsonConvert.DeserializeObject<T>(json);
                }
                catch { }
            }

            return ret;
        }

        public static string CreateUrl(string url, Dictionary<string, string> paramaters)
        {
            return CreateUrl(url, paramaters, false);
        }

        public static string CreateUrl(string url, Dictionary<string, string> parameters, bool forceClearCache)
        {
            if (forceClearCache)
            {
                parameters.Add("cachee", DateTime.Now.Ticks.ToString());
            }

            return string.Format(
                "{0}?{1}",
                url,
                string.Join("&", parameters.Select(x => string.Format("{0}={1}", x.Key, HttpUtility.UrlEncode(x.Value))).ToArray<string>()));
        }

        internal static void AddCookies(ref HttpWebRequest hWebRequest, CookieCollection cookies)
        {
            if (cookies != null)
            {
                hWebRequest.Headers["Cookie"] = "";
                foreach (Cookie cookie in cookies)
                {
                    if (!hWebRequest.Headers["Cookie"].Equals(""))
                        hWebRequest.Headers["Cookie"] += "; ";

                    hWebRequest.Headers["Cookie"] += String.Format("{0}={1}", cookie.Name, cookie.Value);
                }
            }
        }
    }
}
