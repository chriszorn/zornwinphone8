﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Net;
using System.Linq;
using System.Text;

namespace ZornWinPhone8
{
    public class PersistentCookieContainer
    {
        string _name;
        CookieContainer _value;
        CookieContainer _defaultValue;
        bool _hasValue;
        bool _useDotSubDomain;

        SerializedCookieContainer storedValue;

        public PersistentCookieContainer(string name, CookieContainer defaultValue, List<string> domainUris, bool useDotSubDomain)
        {
            this._name = name;
            this._defaultValue = defaultValue;
            this._useDotSubDomain = useDotSubDomain;
            Domains = domainUris;
        }

        public List<string> _domains = new List<string>();
        public List<string> Domains
        {
            get { return _domains; }
            set { _domains = value; }
        }

        public CookieContainer Value
        {
            get
            {
                if (!this._hasValue)
                {
                    if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue(this._name, out this.storedValue))
                    {
                        this._value = this._defaultValue;
                        IsolatedStorageSettings.ApplicationSettings[this._name] = SerializeValue(this._value);
                    }
                    else
                    {
                        this._value = DeSerializeStoredValue(this.storedValue, _useDotSubDomain);
                    }

                    this._hasValue = true;
                }
                return this._value;
            }
            set
            {
                this._value = value;
                this._hasValue = true;

                IsolatedStorageSettings.ApplicationSettings[this._name] = SerializeValue(this._value);
            }
        }

        public CookieContainer DefaultValue
        {
            get { return this._defaultValue; }
        }

        public void ForceRefresh()
        {
            this._hasValue = false;
        }

        private SerializedCookieContainer SerializeValue(CookieContainer value)
        {
            return SerializeCookieContainer(value, Domains);
        }

        public static SerializedCookieContainer SerializeCookieContainer(CookieContainer value, List<string> domains)
        {
            if (value == null)
            {
                return null;
            }

            var serializedCookieContainer = new SerializedCookieContainer();

            // Get the cookie collection for each domain
            foreach (string domain in domains)
            {
                Uri uri = null;

                try
                {
                    uri = new Uri(domain);
                }
                catch { }

                if (uri != null)
                {
                    CookieCollection cookieCollection = value.GetCookies(uri);

                    if (cookieCollection != null && cookieCollection.Count > 0)
                    {
                        SerializedCookie serializedCookie = new SerializedCookie();

                        // Serialize each value in the cookie collection
                        foreach (Cookie cookie in cookieCollection)
                        {
                            if (serializedCookie.ContainsKey(cookie.Name))
                            {
                                serializedCookie[cookie.Name] = cookie.Value;
                            }
                            else
                            {
                                serializedCookie.Add(cookie.Name, cookie.Value);
                            }
                        }

                        serializedCookieContainer.Add(domain, serializedCookie);
                    }
                }
            }

            return serializedCookieContainer;
        }

        public static CookieContainer DeSerializeStoredValue(SerializedCookieContainer storedValue, bool useDotSubDomain)
        {
            if (storedValue == null)
            {
                return null;
            }

            CookieContainer cookieContainer = new CookieContainer();

            foreach (KeyValuePair<string, SerializedCookie> domainCookiePair in storedValue)
            {
                Uri domain = new Uri(domainCookiePair.Key.Replace("http://www.", "http://"));

                foreach (KeyValuePair<string, string> cookieEntry in domainCookiePair.Value)
                {
                    try
                    {
                        if (useDotSubDomain)
                        {
                            cookieContainer.Add(domain, new Cookie(cookieEntry.Key, cookieEntry.Value) { Domain = domain.Host });
                        }
                        else
                        {
                            cookieContainer.Add(domain, new Cookie(cookieEntry.Key, cookieEntry.Value));
                        }
                    }
                    catch { }
                }
            }

            return cookieContainer;
        }
    }

    public class SerializedCookieContainer : Dictionary<string, SerializedCookie>
    {
    }

    public class SerializedCookie : Dictionary<string, string>
    {
    }
}
