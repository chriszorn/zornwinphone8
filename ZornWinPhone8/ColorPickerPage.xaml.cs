﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone8
{
    public partial class ColorPickerPage : PhoneApplicationPage
    {
        private Controls.ColorPicker dataContext
        {
            get { return DataContext as Controls.ColorPicker; }
        }

        public ColorPickerPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (dataContext != null && dataContext.Fill != null)
            {
                PageColorPicker.Color = dataContext.Fill.Color;
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            if(dataContext != null)
            {
                dataContext.Fill = new System.Windows.Media.SolidColorBrush(PageColorPicker.Color);
            }
        }
    }
}