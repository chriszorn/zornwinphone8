﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Text;

using System.Windows;

/* http://blogs.msdn.com/b/andypennell/archive/2010/11/01/error-reporting-on-windows-phone-7.aspx */

namespace ZornWinPhone8
{
    public class LittleWatson
    {
        public static string Email = "chris.zorn@live.com";
        public static string AppName = string.Empty;
        private static List<string> debugLines = new List<string>();

        const int maxBodyLength = 500000;
        const int maxEmailBodyLength = 30000;       // Email body cannot exceed 64K
        const string filename = "LittleWatson.txt";

        public static string LogContents
        {
            get
            {
                return string.Join("\n", LittleWatson.debugLines);
            }
        }

        public static void ReportException(Exception ex, string extra)
        {
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    SafeDeleteFile(store);
                    using (TextWriter output = new StreamWriter(store.CreateFile(filename)))
                    {
                        output.WriteLine();
                        if (!string.IsNullOrWhiteSpace(extra))
                        {
                            output.WriteLine(extra);
                        }

                        try
                        {
                            output.WriteLine("\nPhone:");
                            output.WriteLine(Analytics.AnalyticsProperties.Device);
                            output.WriteLine(Analytics.AnalyticsProperties.OsVersion);
                        }
                        catch { }

                        try
                        {
                            if (LittleWatson.debugLines.Count > 0)
                            {
                                output.WriteLine("\nLog:");
                                output.WriteLine(string.Join("\n", LittleWatson.debugLines));
                            }
                        }
                        catch { }

                        if (ex.InnerException != null)
                        {
                            output.WriteLine("Inner Message:");
                            output.WriteLine(ex.InnerException.Message);

                            output.WriteLine("\nStackTrace:");
                            output.WriteLine(ex.InnerException.StackTrace);
                        }

                        output.WriteLine("\nMessage:");
                        output.WriteLine(ex.Message);
                        output.WriteLine("\nStackTrace:");
                        output.WriteLine(ex.StackTrace);
                        output.Close();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public static void CheckForPreviousException()
        {
            try
            {
                string contents = null;
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists(filename))
                    {
                        using (TextReader reader = new StreamReader(store.OpenFile(filename, FileMode.Open, FileAccess.Read, FileShare.None)))
                        {
                            contents = reader.ReadToEnd();
                        }
                        SafeDeleteFile(store);
                    }
                }
                if (contents != null)
                {
                    SendExceptionToWebpage(contents);

                    string emailContents = contents;

                    if (emailContents.Length > maxEmailBodyLength)
                    {
                        emailContents = emailContents.Substring(0, maxEmailBodyLength);
                    }

                    if (MessageBox.Show("Help me make this app better. Email me the error report!", "Problem Report", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        EmailComposeTask email = new EmailComposeTask();
                        email.To = Email;
                        email.Subject = string.Format("Crash Report: {0}", AppName);

                        email.Body = "If you can, please describe what you were doing when the app crashed.\n\n" + emailContents;
                        SafeDeleteFile(IsolatedStorageFile.GetUserStoreForApplication()); // line added 1/15/2011
                        email.Show();
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                SafeDeleteFile(IsolatedStorageFile.GetUserStoreForApplication());
            }
        }

        /// <summary>
        /// Add debug text to the stack of debug text output with the exception log
        /// </summary>
        /// <param name="debug"></param>
        public static void Log(string debug)
        {
            if (!string.IsNullOrWhiteSpace(debug))
            {
                try
                {
                    LittleWatson.debugLines.Add(debug);
                    System.Diagnostics.Debug.WriteLine(debug);
                }
                catch { }
            }
        }

        public static void ClearLog()
        {
            LittleWatson.debugLines.Clear();
        }

        private static void SendExceptionToWebpage(string contents)
        {
            try
            {
                string subject = string.Format("Crash Report: {0}", AppName);

                SendEmailToMe(subject, contents);
            }
            catch (Exception) { }
        }

        public static void SendLog()
        {
            SendLog(string.Empty);
        }

        public static void SendLog(string subject)
        {
            string content = string.Empty;

            if (LittleWatson.debugLines.Count > 0)
            {
                try
                {
                    content = string.Join("\n", LittleWatson.debugLines);
                }
                catch
                {
                    return;
                }
            }

            SendEmailToMe(string.Format("Log: {0} {1} {2}", AppName, subject, Guid.NewGuid().ToString()), content);
        }

        public static async void SendEmailToMe(string subject, string body)
        {
            //subject = System.Net.HttpUtility.UrlEncode(subject);
            //body = System.Net.HttpUtility.UrlEncode(body);

            if (body.Length > maxBodyLength)
            {
                body = body.Substring(0, maxBodyLength);
            }

            subject = System.Net.HttpUtility.UrlEncode(subject);
            body = System.Net.HttpUtility.UrlEncode(body);

            string resp = await Net.NetUtil.HttpPostRequest(
                "http://zornchris.com/wp-error.php",
                new Dictionary<string, string>()
                {
                    { "subject", subject },
                    { "body", body },
                });
        }

        private static void SafeDeleteFile(IsolatedStorageFile store)
        {
            try
            {
                store.DeleteFile(filename);
            }
            catch (Exception)
            {
            }
        }
    }
}
