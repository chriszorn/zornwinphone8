﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone8
{
    public partial class SaveCancelPage : PhoneApplicationPage
    {
        public SaveCancelPage()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBar.Buttons.Add(new ApplicationBarIconButton() { Text = "save", IconUri = new Uri("/Assets/Icons/check.png", UriKind.Relative), IsEnabled = true });
            ApplicationBar.Buttons.Add(new ApplicationBarIconButton() { Text = "cancel", IconUri = new Uri("/Assets/Icons/cancel.png", UriKind.Relative), IsEnabled = true });

            this.Loaded += SaveCancelPage_Loaded;
        }

        void SaveCancelPage_Loaded(object sender, RoutedEventArgs e)
        {
            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Click -= (DataContext as SaveCancelViewModel).SaveButton_Click;
            (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Click -= (DataContext as SaveCancelViewModel).CancelButton_Click;

            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Click += (DataContext as SaveCancelViewModel).SaveButton_Click;
            (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Click += (DataContext as SaveCancelViewModel).CancelButton_Click;
        }
    }
}