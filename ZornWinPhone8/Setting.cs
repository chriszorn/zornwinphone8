﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;

namespace WindowsPhoneApp
{
    // Encapsulates a key/value pair stored in Isolated Storage ApplicationSettings
    public class Setting<T>
    {
        string name;
        T value;
        T defaultValue;
        bool hasValue;
        public Setting(string name, T defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }
        public T Value
        {
            get
            {
                // Check for the cached value
                if (!this.hasValue)
                {
                    //Deployment.Current.Dispatcher.BeginInvoke(() =>
                    //{
                        // Try to get the value from Isolated Storage
                        if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue(
                        this.name, out this.value))
                        {
                            // It hasn’t been set yet
                            this.value = this.defaultValue;
                            IsolatedStorageSettings.ApplicationSettings[this.name] = this.value;
                        }
                        this.hasValue = true;
                   // });
                }
                return this.value;
            }
            set
            {
                // Save the value to Isolated Storage
                IsolatedStorageSettings.ApplicationSettings[this.name] = value;
                this.value = value;
                this.hasValue = true;
                //Util.DebugLog("Saving settings {" + this.name + " = " + (value as Region).Code + "}");
            }
        }
        public T DefaultValue
        {
            get { return this.defaultValue; }
        }
        // “Clear” cached value:
        public void ForceRefresh()
        {
            this.hasValue = false;
        }
    }
}