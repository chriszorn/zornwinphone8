﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZornWinPhone8
{
    public class HtmlRichTextBox : RichTextBox
    {
        static int height = 50;

        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(HtmlElement[]), typeof(RichTextBox), new PropertyMetadata(new HtmlElement[] { }, ContentPropertyChanged));

        public HtmlElement[] Content
        {
            get { return (HtmlElement[])GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty StyleRulesProperty =
            DependencyProperty.Register("StyleRules", typeof(HtmlElementStyleRule[]), typeof(RichTextBox), new PropertyMetadata(new HtmlElementStyleRule[] { }, StyleRulesPropertyChanged));

        public HtmlElementStyleRule[] StyleRules
        {
            get { return (HtmlElementStyleRule[])GetValue(StyleRulesProperty); }
            set { SetValue(StyleRulesProperty, value); }
        }

        private static void ContentPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var richTextBox = (HtmlRichTextBox)dependencyObject;
            HtmlElement[] content = dependencyPropertyChangedEventArgs.NewValue as HtmlElement[];

            Update(richTextBox, content, richTextBox.StyleRules);
        }

        private static void StyleRulesPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var richTextBox = (HtmlRichTextBox)dependencyObject;
            HtmlElementStyleRule[] styles = dependencyPropertyChangedEventArgs.NewValue as HtmlElementStyleRule[];

            Update(richTextBox, richTextBox.Content, styles);
        }

        private static void Update(HtmlRichTextBox richTextBox, HtmlElement[] content, HtmlElementStyleRule[] styles)
        {
            richTextBox.Blocks.Clear();

            foreach (Block b in HtmlElementsToRichTextBox(content, styles))
            {
                richTextBox.Blocks.Add(b);
            }
        }

        private static List<Block> HtmlElementsToRichTextBox(HtmlElement[] elements, HtmlElementStyleRule[] styles)
        {
            if (elements == null)
            {
                return new List<Block>();
            }

            List<Block> blocks = new List<Block>();

            for (int i = 0; i < elements.Length; i++)
                //foreach(HtmlElement element in elements)
            {
                HtmlElement element = elements[i];
                Paragraph paragraph = new Paragraph();

                // Apply style rules
                var matchingStyle = styles.Where(x => x.Tag == element.Tag && x.Class == element.Class).FirstOrDefault();
                if (matchingStyle != null)
                {
                    matchingStyle.Apply(paragraph);
                }

                if (element.ObjectClassName == "Text")
                {
                    // If the last element was a link, don't create a new paragraph, append 
                    // this text to the prior paragraph
                    if (blocks.Count > 0
                        && i > 0
                        && elements[i - 1].ObjectClassName == "TextLink")
                    {
                        (blocks.Last() as Paragraph).Inlines.Add(new Run() { Text = element.Text });
                    }
                    else
                    {
                        paragraph.Inlines.Add(new Run() { Text = element.Text });
                        blocks.Add(paragraph);
                    }
                }
                if (element.ObjectClassName == "BoldText")
                {
                    // If the last element was a link, don't create a new paragraph, append 
                    // this text to the prior paragraph
                    if (blocks.Count > 0
                        && i > 0
                        && elements[i - 1].ObjectClassName == "TextLink")
                    {
                        (blocks.Last() as Paragraph).Inlines.Add(new Run() { Text = element.Text, FontWeight = FontWeights.Bold });
                    }
                    else
                    {
                        paragraph.Inlines.Add(new Run() { Text = element.Text, FontWeight = FontWeights.Bold });
                        blocks.Add(paragraph);
                    }
                }
                else if (element.ObjectClassName == "Image")
                {
                    InlineUIContainer inline = new InlineUIContainer();
                    Image image = new Image()
                    {
                        Source = new BitmapImage() { UriSource = new Uri(element.Source) },
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(0,6,0,6)
                    };
                    image.ImageOpened += image_ImageOpened;

                    //Binding maxHeightBinding = new Binding("MaxHeightProperty");
                    //maxHeightBinding.Source = paragraph;// image.Height;
                    //image.SetBinding(Image.MaxHeightProperty, maxHeightBinding);

                    inline.Child = image;

                    paragraph.Inlines.Add(inline);
                    paragraph.TextAlignment = TextAlignment.Center;
                    blocks.Add(paragraph);
                }
                else if (element.ObjectClassName == "TextLink")
                {
                    Hyperlink a = new Hyperlink();
                    a.NavigateUri = new Uri(element.Url);
                    a.TargetName = "_blank";
                    a.Inlines.Add(new Run() { Text = element.Text, Foreground = Application.Current.Resources["PhoneAccentBrush"] as SolidColorBrush });

                    if (blocks.Count > 0)
                    {
                        (blocks.Last() as Paragraph).Inlines.Add(a);
                    }
                    else
                    {
                        paragraph.Inlines.Add(a);
                        blocks.Add(paragraph);
                    }
                }
            }

            return blocks;
        }

        static void image_ImageOpened(object sender, RoutedEventArgs e)
        {
            // Set the maxwidth of the image to the actual width of the image.
            // This prevents the image from being scaled up and becoming blurry
            double pixelWidth = ((sender as Image).Source as BitmapImage).PixelWidth;
            (sender as Image).MaxWidth = pixelWidth;
        }
    }
}
