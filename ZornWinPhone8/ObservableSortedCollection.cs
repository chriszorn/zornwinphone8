﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ZornWinPhone8
{
    public class ObservableSortedCollection<T> : ObservableCollection<T>
    {
        private Comparison<T> comparison;

        public ObservableSortedCollection()
        {
            comparison = null;
        }

        public ObservableSortedCollection(Comparison<T> comparison) 
        {
            this.comparison = comparison;
        }

        new public void Add(T item) {

            if (comparison == null)
            {
                Insert(Items.Count, item);
                return;
            }

            if (Items.Count == 0)
                Insert(0, item);
            else
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (comparison(Items[i], item) >= 1)
                    {
                        Insert(i, item);
                        return;
                    }
                }
                Insert(Items.Count, item);
            }
        }
    }
}
