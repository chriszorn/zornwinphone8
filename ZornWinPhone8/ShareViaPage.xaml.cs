﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace ZornWinPhone8
{
    public partial class ShareViaPage : PhoneApplicationPage
    {
        public static ShareTaskData ShareTaskData;

        public ShareViaPage()
        {
            InitializeComponent();
            ShareOptions.SelectedIndex = 0;
        }

        void ShareOptions_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            switch (ShareOptions.SelectedIndex)
            {
                case 0:
                    ShareLinkTask shareLinkTask = new ShareLinkTask();
                    shareLinkTask.LinkUri = new Uri(ShareTaskData.Url);
                    shareLinkTask.Message = ShareTaskData.Message;
                    shareLinkTask.Title = ShareTaskData.Title;
                    shareLinkTask.Show();
                    break;

                case 1:
                    SmsComposeTask smsComposeTask = new SmsComposeTask();
                    smsComposeTask.Body = ShareTaskData.Title + "\n" + ShareTaskData.Message + "\n" + ShareTaskData.Url;
                    smsComposeTask.Show();
                    break;

                case 2:
                    EmailComposeTask emailComposeTask = new EmailComposeTask();
                    emailComposeTask.Subject = ShareTaskData.Title;
                    emailComposeTask.Body = ShareTaskData.Message + "\n\n" + ShareTaskData.Url;
                    emailComposeTask.Show();
                    break;

                case 3:
                    Clipboard.SetText(ShareTaskData.Url);
                    NavigationService.GoBack();
                    break;
            }
        }
    }

    public class ShareTaskData
    {
        public string Title = string.Empty;
        public string Url;
        public string Message = string.Empty;
    }

    public class ShareOptions : List<String> { }
}