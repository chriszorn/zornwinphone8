﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone8.Net
{
    public static class NetUtil
    {
        public static async Task<NetResponse> GetResponseAsync(HttpWebRequest request)
        {
            HttpWebResponse response = null;
            NetResponse ret = null;

            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                ret = new NetResponse()
                {
                    Content = ResponseToString(response),
                    Response = response,
                    StatusCode = response.StatusCode,
                };
            }
            catch (WebException e)
            {
                response = (HttpWebResponse)e.Response;

                if (response != null)
                {
                    ret = new NetResponse()
                    {
                        Content = ResponseToString(response),
                        Response = response,
                        StatusCode = response.StatusCode,
                    };
                }
                else
                {
                    ret = new NetResponse()
                    {
                        Content = string.Empty,
                        Response = null,
                        StatusCode = HttpStatusCode.Conflict,
                    };
                }
            }

            return ret;
        }

        public static async Task<string> HttpGetRequest(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        public static async Task<HttpWebResponse> HttpCookieContainerGetRequestResponse(string url, CookieContainer cookies)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.CookieContainer = cookies;
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                return response;
            }
            catch
            {
                return default(HttpWebResponse);
            }
        }

        public static async Task<HttpWebResponse> HttpGetRequestResponse(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                return response;
            }
            catch(Exception e)
            {
                return default(HttpWebResponse);
            }
        }

        public static async Task<string> HttpPostRequest(string url, Dictionary<string, string> data)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                Stream postStream = await request.GetRequestStreamAsync();
                byte[] bytes = Encoding.UTF8.GetBytes(string.Join("&", data.Select(x => string.Format("{0}={1}", x.Key, x.Value))));
                postStream.Write(bytes, 0, bytes.Length);
                postStream.Close();

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        public static async Task<HttpWebResponse> HttpPostRequestResponse(string url, Dictionary<string, string> data)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                Stream postStream = await request.GetRequestStreamAsync();
                byte[] bytes = Encoding.UTF8.GetBytes(string.Join("&", data.Select(x => string.Format("{0}={1}", x.Key, x.Value))));
                postStream.Write(bytes, 0, bytes.Length);
                postStream.Close();

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();

                return response;
            }
            catch (Exception e)
            {
                return default(HttpWebResponse);
            }
        }

        public static async Task<HttpWebResponse> HttpPostCookieRequestResponse(string url, Dictionary<string, string> data, CookieContainer cookies)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CookieContainer = cookies;
                //AddCookies(ref request, cookies);

                Stream postStream = await request.GetRequestStreamAsync();
                byte[] bytes = Encoding.UTF8.GetBytes(string.Join("&", data.Select(x => string.Format("{0}={1}", x.Key, x.Value))));
                postStream.Write(bytes, 0, bytes.Length);
                postStream.Close();

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                return response;
            }
            catch(Exception e)
            {
                return default(HttpWebResponse);
            }
        }

        public static async Task<HttpWebResponse> HttpPostCookieRequestResponse(string url, Dictionary<string, string> data, string cookies)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Headers["Cookie"] = cookies;

                Stream postStream = await request.GetRequestStreamAsync();
                byte[] bytes = Encoding.UTF8.GetBytes(string.Join("&", data.Select(x => string.Format("{0}={1}", x.Key, x.Value))));
                postStream.Write(bytes, 0, bytes.Length);
                postStream.Close();

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                return response;
            }
            catch (Exception e)
            {
                return default(HttpWebResponse);
            }
        }

        public static async Task<byte[]> HttpGetRequestBytes(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                MemoryStream ms = new MemoryStream();
                using (Stream input = response.GetResponseStream())
                {
                    input.CopyTo(ms);
                }
                return ms.ToArray();
            }
            catch
            {
                return default(byte[]);
            }
        }

        public static async Task<Stream> HttpGetRequestStream(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                return response.GetResponseStream();
            }
            catch
            {
                return default(Stream);
            }
        }

        public static async Task<string> HttpCookieGetRequest(string url, CookieCollection cookies)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                AddCookies(ref request, cookies);
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        public static async Task<T> HttpGetRequestToDeserializedJson<T>(string url)
        {
            T ret = default(T);
            string json = await HttpGetRequest(url);
            ret = SafeJsonDeserialize<T>(json);
            return ret;
        }

        public static T SafeJsonDeserialize<T>(string json)
        {
            T ret = default(T);

            if (!string.IsNullOrWhiteSpace(json))
            {
                try
                {
                    ret = JsonConvert.DeserializeObject<T>(json);
                }
                catch (Exception e) { }
            }

            return ret;
        }

        public static string CreateUrl(string url, Dictionary<string, string> paramaters)
        {
            return CreateUrl(url, paramaters, false);
        }

        public static string CreateUrl(string url, Dictionary<string, string> parameters, bool forceClearCache)
        {
            if (forceClearCache)
            {
                parameters.Add("cachee", DateTime.Now.Ticks.ToString());
            }

            return string.Format(
                "{0}?{1}",
                url,
                string.Join("&", parameters.Select(x => string.Format("{0}={1}", x.Key, HttpUtility.UrlEncode(x.Value))).ToArray<string>()));
        }

        public static string ResponseToString(HttpWebResponse response)
        {
            try
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        internal static void AddCookies(ref HttpWebRequest hWebRequest, CookieCollection cookies)
        {
            if (cookies != null)
            {
                hWebRequest.Headers["Cookie"] = "";
                foreach (Cookie cookie in cookies)
                {
                    if (!hWebRequest.Headers["Cookie"].Equals(""))
                        hWebRequest.Headers["Cookie"] += "; ";

                    hWebRequest.Headers["Cookie"] += String.Format("{0}={1}", cookie.Name, cookie.Value);
                }
            }
        }
    }
}
