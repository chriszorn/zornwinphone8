﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ZornWinPhone8.UnitTestApp.Resources;
using System.Threading;
using Microsoft.VisualStudio.TestPlatform.Core;
using vstest_executionengine_platformbridge;
using Microsoft.VisualStudio.TestPlatform.TestExecutor;
using System.Reflection;

namespace ZornWinPhone8.UnitTestApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            var wrapper = new TestExecutorServiceWrapper();
            new Thread(new ServiceMain((param0, param1) => wrapper.SendMessage((ContractName)param0, param1)).Run).Start();

        }
    }

    public class ViewModel : ZornWinPhone8.ViewModelBase
    {
        private HtmlElement[] _htmlElements;
        public HtmlElement[] HtmlElements
        {
            get { return _htmlElements; }
            set { _htmlElements = value; NotifyPropertyChanged(); }
        }

        public ViewModel()
        {
            string json = "[{\"Source\":\"http://beta.na.leagueoflegends.com/sites/default/files/styles/wide_medium/public/upload/lol.com_.articleheader.endofseason3rewards.enawea.jpg?itok=iLyn2uSf\",\"Tag\":\"img\",\"Class\":\"\",\"ObjectClassName\":\"Image\"},{\"Text\":\"Announcing end of Season 3 rewards\",\"Tag\":\"h1\",\"Class\":\"article-title\",\"ObjectClassName\":\"Text\"},{\"Text\":\"By Magus\",\"Tag\":\"span\",\"Class\":\"posted_by\",\"ObjectClassName\":\"Text\"},{\"Text\":\"With less than three months to go until the end of Season 3, we know anticipation is growing around this year\u2019s rewards. It\u2019s time to reveal what you can earn for Season 3 ranked play.\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Text\":\"We\u2019re dishing out the rewards based on the tier you reach at the end of Season 3 ranked play. You\u2019ll earn rewards by playing ranked in any game mode (solo, duo, 5v5 or 3v3), and you\u2019ll nab the reward corresponding to the highest tier you hit across all modes. Keep in mind that for 5v5 and 3v3 tier rewards, you have to have played at least five games with the team and participated in 30% or more of the teams total games to qualify.\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Text\":\"Enough with the exposition, let\u2019s show off the rewards!\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Text\":\"Tier Rewards\",\"Tag\":\"h3\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Text\":\"(Based on the tier you\u2019re in at the end of Season 3)\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Source\":\"http://riot-web-static.s3.amazonaws.com/images/news/August_2013/EOSR/rewards-bronze-crest.png\",\"Tag\":\"img\",\"Class\":\"\",\"ObjectClassName\":\"Image\"},{\"Text\":\"Bronze or higher: Season 3 Summoner Icon\\n\",\"Tag\":\"strong\",\"Class\":\"\",\"ObjectClassName\":\"BoldText\"},{\"Text\":\"You\u2019ll earn a Summoner Icon that displays your accomplishments. There is a different icon for each tier.\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Source\":\"http://riot-web-static.s3.amazonaws.com/images/news/August_2013/EOSR/rewards-silver-crest.png\",\"Tag\":\"img\",\"Class\":\"\",\"ObjectClassName\":\"Image\"},{\"Text\":\"Silver or higher: Profile banner trim, Loading screen border, and Victorious Ward Skin\\n\",\"Tag\":\"strong\",\"Class\":\"\",\"ObjectClassName\":\"BoldText\"},{\"Text\":\"From your profile banner to your loading screen, reach Silver or higher to earn the corresponding trim. Each tier has a different colored trim. At Silver tier, you\u2019ll also pick up a new permanent Victorious Ward skin to light up the jungle your way.\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Source\":\"http://riot-web-static.s3.amazonaws.com/images/news/August_2013/EOSR/rewards-goldplatdiam-crests.png\",\"Tag\":\"img\",\"Class\":\"\",\"ObjectClassName\":\"Image\"},{\"Text\":\"Gold or higher: Season 3 medal for Buddy and Team invitations and Victorious Champion Skin\\n\",\"Tag\":\"strong\",\"Class\":\"\",\"ObjectClassName\":\"BoldText\"},{\"Text\":\"Should you climb to the highest ranks of competitive play, you\u2019ll greet new friends and teammates with a personalized invitation badge that shows off your success in shiny style. You\u2019ll also earn the permanent Victorious Ward skin and a unique Victorious skin.\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Text\":\"If you have questions or want to learn more about end of season rewards,\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Url\":\"http://beta.na.leagueoflegends.com/en/check-out-season-3-rewards-faq\",\"Text\":\"check out our handy FAQ\",\"Tag\":\"a\",\"Class\":\"\",\"ObjectClassName\":\"TextLink\"},{\"Text\":\".\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Text\":\"Whether you spin up a new web of teammates or skitter your way to the victory with old friends, head into ranked teams for the chance to earn a viciously victorious new skin as a reward. You\u2019ve got until 10/31, so we\u2019ll see you on the Fields of Justice! GLHF\",\"Tag\":\"p\",\"Class\":\"\",\"ObjectClassName\":\"Text\"},{\"Source\":\"http://riot-web-static.s3.amazonaws.com/images/news/August_2013/EOSR/Elise-Victorious-Face-EN_thumb.jpg\",\"Tag\":\"img\",\"Class\":\"\",\"ObjectClassName\":\"Image\"},{\"Text\":\"17 hours ago\",\"Tag\":\"span\",\"Class\":\"time-ago time-ago-node-4709\",\"ObjectClassName\":\"Text\"},{\"Url\":\"http://l.com/en/news/game-updates/features\",\"Text\":\"Features\",\"Tag\":\"a\",\"Class\":\"\",\"ObjectClassName\":\"TextLink\"},{\"Text\":\"Tagged with: \",\"Tag\":\"div\",\"Class\":\"field-label\",\"ObjectClassName\":\"Text\"},{\"Url\":\"http://l.com/en/tag/season-3\",\"Text\":\"season 3\",\"Tag\":\"a\",\"Class\":\"\",\"ObjectClassName\":\"TextLink\"},{\"Text\":\",\",\"Tag\":\"div\",\"Class\":\"field field-name-field-tags field-type-taxonomy-term-reference field-label-inline\",\"ObjectClassName\":\"Text\"},{\"Url\":\"http://l.com/en/tag/rewards\",\"Text\":\"rewards\",\"Tag\":\"a\",\"Class\":\"\",\"ObjectClassName\":\"TextLink\"}]";
            HtmlElements = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlElement[]>(json);
        }
    }
}